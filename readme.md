# Portal boliviasolidaria.org.bo

Página de bolivia solidaria (boliviasolidaria.org.bo).

BoliviaSolidaria es una plataforma para gestionar acciones de colaboración entre ciudadanos desde cualquier lugar de Bolivia.

## Sobre el proyecto

### ¿Quiénes pueden usar la plataforma? 

La plataforma puede ser usada por cualquier persona con acceso a internet y desde cualquier dispositivo, que requiera u ofrezca ayuda. Una vez enviada la solicitud, pasará a un proceso de revisión para luego ser publicada en la página. Aquí el solicitante debe ser lo más específico con su necesidad u oferta de ayuda .

### Por qué hacemos voluntariado en Bolivia Solidaria?

Durante los últimos años en Bolivia hemos vividos diferentes tipos de sucesos como desastres naturales, deslizamientos, sequías etc. En tiempos de crisis, es necesario contar con ayuda oportuna y queremos aportar en la construcción de una red que lo permita.

Muchas personas pueden brindar ayuda, pero no saben cómo ni donde, mediante la plataforma podrán ver las necesidades en las que pueden focalizar su ayuda y ser parte de la solución.

Asimismo, los Organismos de ayuda humanitaria necesitan una plataforma que les facilite información para distribuir la ayuda y llegar a los damnificados de manera oportuna.

### ¿Quiénes pueden usar la plataforma? 
La plataforma puede ser usada por cualquier persona con acceso a internet y desde cualquier dispositivo, que requiera u ofrezca ayuda. Una vez enviada la solicitud, pasará a un proceso de revisión para luego ser publicada en la página. Aquí el solicitante debe ser lo más específico con su necesidad u oferta de ayuda .
 
¿Qué se puede hacer en la página?
En la página https://boliviasolidaria.org.bo se pueden realizar reportes de:

- Solicitudes de ayuda
- Ofertas de ayuda 
- Servicios de salud
- Servicios públicos disponibles

## Sobre el desarrollo del sitio web

El sitio se conecta al servicio de [ushahidi](https://www.ushahidi.com/), este servicio es una plataforma donde se registra información como solicitudes y ofertas de ayuda, servicios de salud y otros servicios públicos. El servicio de ushahidi tiene un mapa y permite llenar un formulario con solicitudes, estas solicitudes van a una bandeja donde es revisada por las personas que administran la cuenta de ushahidi.

La página es estática y tiene un iframe que muestra el mapa de ushahidi correspondiente a boliviasolidaria, pero el [api de ushahidi](https://docs.ushahidi.com/platform-developer-documentation/tech-stack/api-documentation) esta bien documentado y se podría usar en un frontend propio. 

### Licencia de software

Liberado bajo licencia AGPL ver [LICENSE.md](LICENSE.md) <noemi_angles@riseup.net>

### Sobre la migración del servicio de ushahidi

El servicio de ushahidi es de pruebas y expirará en unas semanas, se puede instalar una instancia de ushahidi en un servidor de nuestro control y asegurar la permanencia del servicio.

### Instalación

Solo hay que clonar este proyecto y abrir el `index.html` en un navegador.

### Tareas pendientes (TODO list)

La lista de tareas se maneja en un [kanban de taiga](https://tree.taiga.io/project/nicovw-bolivia-solidaria-dev/kanban) si tienes sugerencias puedes llenar issues aquí o en el kanban. Para participar [https://boliviasolidaria.org.bo/contact.html](https://boliviasolidaria.org.bo/contact.html).
